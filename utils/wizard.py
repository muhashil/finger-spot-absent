import os
import pandas as pd

from flask import current_app

def calculate_worked_hours(filename):
    df = pd.read_csv(os.path.join(current_app.config['UPLOAD_FOLDER'], filename), sep=',')
    deleted_columns = ['PIN', 'Serial Number', 'Location', 
                    'Type', 'Status', 'Date Received']
    for column in deleted_columns:
        if column in df:
            del df[column]

    df.loc[:, 'Date'] = pd.to_datetime(df['Date Time']).dt.date
    df.loc[:, 'Date Time'] = pd.to_datetime(df['Date Time'])
    output = {}

    names = list(set(df['Name']))
    err = []
    for name in names:
        filter_data_by_name = df['Name'] == name
        df_user_groups = df[filter_data_by_name].groupby('Date')
        total_worked_hours = 0

        for (group_name, df_user_group) in df_user_groups:
            df_user_group = df_user_group[::-1]

            if (len(df_user_group) % 2) != 0:
                df_user_group.loc[:, 'W.M'] = None
                df_user_group.loc[:, 'W.H'] = None
                df_user_group.loc[:, 'Correct'] = False
                
                err.append(df_user_group.to_dict())
                # print(df_user_group)
            else:
                df_user_time_range = df_user_group['Date Time'] - df_user_group['Date Time'].shift(1)
                df_user_group.loc[:, 'W.M'] = (df_user_time_range).astype('timedelta64[m]')
                df_user_group.loc[:, 'W.H'] = 0

                for number in range(1, len(df_user_group), 2):
                    df_user_group.loc[:, 'W.H'] = df_user_group['W.H'] + df_user_group.loc[df_user_group.index[number], 'W.M']
                
                # convert minutes into hours
                df_user_group.loc[:, 'W.H'] = df_user_group['W.H'] / 60
                df_user_group.loc[:, 'Correct'] = True

                # add total worked hours
                total_worked_hours += df_user_group.loc[df_user_group.index[0], 'W.H']

            # print(df_user_group)

        output.update({name: total_worked_hours})

    return output, err

