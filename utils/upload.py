
ALLOWED_EXTENSIONS = {'csv'}

def check_file(filename):
    if '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS:
        return True
    return False