import os
import pandas as pd

from werkzeug.utils import secure_filename
from flask import Flask, flash, render_template, request, redirect

from utils.wizard import calculate_worked_hours
from utils.upload import check_file

UPLOAD_FOLDER = os.path.join(os.path.dirname((os.path.abspath(__file__))), 'uploads')

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config.from_mapping(
    SECRET_KEY='absensi'
)

@app.template_filter()
def pretty_hours(hours):
    return round(hours, ndigits=2)

@app.route('/', methods=['GET', 'POST'])
def index():
    context = {}
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file uploaded.')
            return redirect(request.url)

        file = request.files['file']
        if file.filename == '':
            flash('No selected file.')
            return redirect(request.url)

        if file and check_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            worked_hours, err = calculate_worked_hours(filename)
            context = {
                'filename': filename,
                'worked_hours': worked_hours,
                'error': err if len(err) > 0 else None
            }
            return render_template('index.html', context=context)
    return render_template('index.html')